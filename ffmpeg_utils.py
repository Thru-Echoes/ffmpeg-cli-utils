import os
import subprocess 

def get_video_to_frames_shell(input_path, start_time, end_time, output_path, fps = 30):
    """
    Returns list of CLI ffmpeg call

    Parameters
    ----------
    input_path: str
       File path of video to process
    start_time: str
       Starting time of video processing in H:m:s:ms
    end_time: str
       Ending time of video processing in H:m:s:ms
    output_path: str
       File path to create images
    fps: int, opt
       Frames per second of video to process
    """
    return ["ffmpeg",
            "-loglevel",
            "verbose",
            "-ss",
            start_time,
            "-to",
            end_time,
            "-i",
            input_path,
            "-vf",
            "fps=" + str(fps),
            "-q:v",
            "1",
            output_path + "/frame_%09d.jpg"]

shell_cmd = get_video_to_frames_shell(input_path = "path/to/my/video",
                                      start_time = "00:00:00.000",
                                      end_time = "00:05:00.000",
                                      output_path = "path/to/write/imgs",
                                      fps = 60)
subprocess.run(shell_cmd)
