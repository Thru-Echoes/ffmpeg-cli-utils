import sys 
import os 

base_dir = os.path.dirname(os.path.dirname(__file__))
input_path = "path/to/input"
output_path = "path/to/output"

# Confirm dir exists else mkdir 
if (not os.path.exists(output_path)):
    os.mkdir(output_path)

files = os.listdir(input_path)
for input_path in files:
    if not os.path.isfile(input_path + "//" + input_file): continue

    # TODO: run video_to_frames 
    # Run video_to_frames per file in dir structure
